git-status-behind-all
=====================

## About

`git-status-behind-all`, as the name suggests, is a little script to check if the repositories in the current directory aren't lagging behind their remote copies. Literally, the script just loops over the first level subdirectories of the current dir, and then runs `git status` for the active branch. Errors and non-git subdirectories are ignored.

## Usage

Just run it. Best used with the [`git-diff-all`](https://bitbucket.org/dsjkvf/git-diff-all) script, which checks if the repositories in the current directory haven't got any unsubmitted changes compared to their remote copies.
